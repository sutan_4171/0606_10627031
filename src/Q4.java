import java.util.Random;
import java.util.Scanner;

public class Q4 {
    static void UP(int c,int num[]){
        for(int i=c-1;i>=0;i--){
            System.out.print(num[i]+" ");
        }
    }

    static void DOWN(int c,int num[]){
        for(int i=0;i<c;i++){
            System.out.print(num[i]+" ");
        }
    }

    public static void main(String[] args) {
        Random ran = new Random();
        Scanner scanner = new Scanner(System.in);
        int c= ran.nextInt(11)+10;

        System.out.println("請輸入"+c+"個整數");
        int num[] = new int[c];

        for(int i=0;i<c;i++){
            num[i]=scanner.nextInt();
        }
        for(int i=0;i<c-1;i++){
            if(num[i]<num[i+1]){
                int cc=num[i];
                num[i]=num[i+1];
                num[i+1]=cc;
                i=-1;
            }
        }
        for(int i=0;i<c;i++){
            System.out.print(num[i]+" ");
        }
        System.out.println("\n請選擇U(升冪)/D(降冪)");
        String s;
        s=scanner.next();
        System.out.println(s);
        if(s.equals("U")){
            UP(c,num);
        }
        else if (s.equals("D")){
            DOWN(c,num);
        }
    }

}
