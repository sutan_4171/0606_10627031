import java.io.*;
import java.util.Random;

public class Q2{
    public static void main(String[] args)
    {
        Random ran = new Random();
        int c=ran.nextInt(11)+10;
        int num[]= new int[c];
        int a=0,b=0,sum=0;


        File saveFile=new File("output.txt");
        try
        {
            FileWriter fwriter=new FileWriter(saveFile);
            for(int i=0;i<c;i++){
                num[i]=ran.nextInt(10)-5;
                fwriter.write(num[i]+"\n");
            }
            fwriter.flush();
            fwriter.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        try {
            File filename = new File("output.txt"); // 要讀取以上路徑的input。txt檔案
            InputStreamReader reader = new InputStreamReader(
                    new FileInputStream(filename)); // 建立一個輸入流物件reader
            BufferedReader br = new BufferedReader(reader); // 建立一個物件，它把檔案內容轉成計算機能讀懂的語言
            String line = "";
            while (( line =  br.readLine()) != null) {
                if(Integer.valueOf(line)>0) a++;
                else if(Integer.valueOf(line)<0)  b++;
                else if(Integer.valueOf(line)==0) break;
                sum+=Integer.valueOf(line);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        System.out.println("正:"+a+" 負"+b+" 總和"+sum+" 平均"+(float)sum/c);
    }
}