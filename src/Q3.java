import java.util.Random;
import java.util.Scanner;

public class Q3 {
    public static void main(String[] args) {
        Random ran = new Random();
        Scanner scanner = new Scanner(System.in);
        int min=2147483647,max=-2147483648,maxcount=0,mincount=0;
        int c=ran.nextInt(10)+1;

        System.out.println("請輸入"+c+"個整數");
        for(int i=0;i<c;i++){
            int ch=scanner.nextInt();
            if(ch<=min){
                if(ch==min) mincount++;
                else mincount=1;
                min=ch;
            }
            else if(ch>=max){
                if(ch==max) maxcount++;
                else maxcount=1;
                max=ch;
            }
        }
        System.out.println("最大值為"+max+" 出現次數"+maxcount);
        System.out.println("最小值為"+min+" 出現次數"+mincount);
    }
}
